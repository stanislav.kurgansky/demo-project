import React, { useState, useEffect } from 'react';

import api from 'src/api/todoData';

import TodoInput from './subcomponents/TodoInput';
import TodoItem from './subcomponents/TodoItem';

import classes from './TodoList.module.scss';

const Todolist = () => {
    const [todoList, setTodoList] = useState([]);

    const handleAddNewItem = async value => {
        try {
            const updatedTodoList = await api.addTodoItem(value);
            setTodoList(updatedTodoList);
        } catch {
            console.log('Error: Cannot add todo item to the list.');
        }
    };

    const handleMarkComplete = async item => {
        try {
            const updatedTodoList = await api.markComplete(item);
            setTodoList(updatedTodoList);
        } catch {
            console.log('Error: Cannot complete todo item.');
        }
    };

    const handleDeleteItem = async id => {
        try {
            const updatedTodoList = await api.deleteById(id);
            setTodoList(updatedTodoList);
        } catch {
            console.log('Error: Cannot delete todo item.');
        }
    };
    useEffect(() => {
        const fetchData = async () => {
            try {
                const result = await api.getTodoList();
                setTodoList(result);
            } catch {
                console.log('Error: Cannot fetch data from server');
            }
        };
        fetchData();
    }, []);

    return (
        <div className={classes.root}>
            <h1>TodoList - demo app</h1>
            <TodoInput onAddNewItem={handleAddNewItem} />
            <div className={classes.list}>
                {todoList.map(item => {
                    return (
                        <TodoItem
                            key={item.id}
                            item={item}
                            onMarkComplete={handleMarkComplete}
                            onDeleteItem={handleDeleteItem}
                        />
                    );
                })}
            </div>
        </div>
    );
};

export default Todolist;
