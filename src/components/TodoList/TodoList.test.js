import { fireEvent, render, screen } from '@testing-library/react';
import TodoList from './TodoList';

test('should render todo list with provided items', async () => {
    render(<TodoList />);
    expect(await screen.findByText('First Todo')).toBeInTheDocument();
    expect(await screen.findByText('Second Todo')).toBeInTheDocument();
    expect(await screen.findByText('Third Todo')).toBeInTheDocument();
});

test('should add new todo item and render todo list', async () => {
    const renderer = render(<TodoList />);
    const input = renderer.getByLabelText('ui-input');
    const button = renderer.getByText('Add');

    fireEvent.change(input, { target: { value: 'Forth Todo' } });
    fireEvent.click(button);

    expect(await renderer.findByText('First Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Second Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Third Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Forth Todo')).toBeInTheDocument();
});

test('should mark First Todo item complete', async () => {
    const renderer = render(<TodoList />);

    expect(await renderer.findByText('First Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Second Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Third Todo')).toBeInTheDocument();

    const checkbox = renderer.getByTestId('todo-1');

    fireEvent.click(checkbox);
    expect(await (await renderer.findByText('First Todo')).className).toContain('complete');
});

test('should delete Third Todo item and show only first two', async () => {
    const renderer = render(<TodoList />);

    expect(await renderer.findByText('First Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Second Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Third Todo')).toBeInTheDocument();

    const button = renderer.getByTestId('button-todo-3');

    fireEvent.click(button);
    expect(await renderer.findByText('First Todo')).toBeInTheDocument();
    expect(await renderer.findByText('Second Todo')).toBeInTheDocument();
    expect(renderer.queryByText('Third Todo')).not.toBeInTheDocument();
});
