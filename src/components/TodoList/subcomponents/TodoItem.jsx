import React from 'react';
import Button from 'src/UI/Button/Button';
import Checkbox from 'src/UI/Checkbox/Checkbox';
import Text from 'src/UI/Text/Text';

import classes from './TodoItem.module.scss';

const TodoItem = ({ item, onMarkComplete, onDeleteItem }) => {
    const handleChangeCheckbox = () => {
        onMarkComplete({ ...item, complete: true });
    };
    const handleDeleteClick = () => {
        onDeleteItem(item.id);
    };

    return (
        <div className={classes.root} data-testid={`item-${item.id}`}>
            <Checkbox id={item.id} checked={item.complete} onChange={handleChangeCheckbox} label="Complete" />
            <Text className={item.complete ? classes.complete : ''}>{item.name}</Text>
            <Button id={item.id} onClick={handleDeleteClick}>
                Delete
            </Button>
        </div>
    );
};

export default TodoItem;
