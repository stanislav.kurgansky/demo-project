import React, { useState } from 'react';

import Input from 'src/UI/Input/Input';
import Button from 'src/UI/Button/Button';

import classes from './TodoInput.module.scss';

const TodoInput = ({ onAddNewItem }) => {
    const [value, setValue] = useState('');

    const handleChange = e => {
        e.preventDefault();
        const value = e.target.value;
        setValue(value);
    };

    const handleClick = () => {
        if (value) {
            onAddNewItem(value);
            setValue('');
        }
    };

    return (
        <div className={classes.root}>
            <Input value={value} onChange={handleChange} placeholder="Todo name" />

            <Button onClick={handleClick}>Add</Button>
        </div>
    );
};

export default TodoInput;
