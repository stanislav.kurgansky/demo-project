import { fireEvent, render } from '@testing-library/react';
import TodoInput from './TodoInput';

test('should add a new todo item if the input has value.', () => {
    const handleAddNewItem = jest.fn();
    const renderer = render(<TodoInput onAddNewItem={handleAddNewItem} />);
    const input = renderer.getByLabelText('ui-input');
    const button = renderer.getByText('Add');

    fireEvent.change(input, { target: { value: 'New Todo' } });
    expect(input.value).toBe('New Todo');

    fireEvent.click(button);
    expect(handleAddNewItem).toBeCalledTimes(1);
    expect(handleAddNewItem).toBeCalledWith('New Todo');
    expect(input.value).toBe('');
});

test('should not a add new todo item if the input is empty.', () => {
    const handleAddNewItem = jest.fn();
    const renderer = render(<TodoInput onAddNewItem={handleAddNewItem} />);
    const input = renderer.getByLabelText('ui-input');
    const button = renderer.getByText('Add');

    expect(input.value).toBe('');

    fireEvent.click(button);
    expect(handleAddNewItem).toBeCalledTimes(0);
});
