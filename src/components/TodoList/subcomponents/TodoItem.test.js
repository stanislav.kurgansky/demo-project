import { fireEvent, render } from '@testing-library/react';
import TodoItem from './TodoItem';

test('should render incomplete todo item', () => {
    const item = { id: 'todo', name: 'First Todo', complete: false };
    const renderer = render(<TodoItem item={item} />);
    const text = renderer.getByText(item.name);

    expect(text.className).not.toContain('complete');
});

test('should render complete todo item', () => {
    const item = { id: 'todo', name: 'First Todo', complete: true };
    const renderer = render(<TodoItem item={item} />);
    const text = renderer.getByText(item.name);

    expect(text.className).toContain('complete');
});

test('should mark complete todo item', () => {
    const handleMarkComplete = jest.fn();
    const inCompleteItem = { id: 'todo', name: 'First Todo', complete: false };
    const completeItem = { id: 'todo', name: 'First Todo', complete: true };
    const renderer = render(<TodoItem item={inCompleteItem} onMarkComplete={handleMarkComplete} />);
    const checkbox = renderer.getByLabelText('Complete');

    fireEvent.click(checkbox);
    expect(handleMarkComplete).toBeCalledTimes(1);
    expect(handleMarkComplete).toBeCalledWith(completeItem);
});

test('should delete todo item', () => {
    const handleDeleteItem = jest.fn();
    const item = { id: 'todo', name: 'First Todo', complete: false };
    const renderer = render(<TodoItem item={item} onDeleteItem={handleDeleteItem} />);
    const button = renderer.getByText('Delete');

    fireEvent.click(button);
    expect(handleDeleteItem).toBeCalledTimes(1);
    expect(handleDeleteItem).toBeCalledWith(item.id);
});
