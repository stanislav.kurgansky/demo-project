import React from 'react';

import classes from './Text.module.scss';

const Text = ({ className, children }) => {
    return <div className={`${classes.root} ${className ? className : ''}`}>{children}</div>;
};

export default Text;
