import React from 'react';

import classes from './Input.module.scss';

const Input = ({ value, placeholder, onChange }) => {
    return (
        <input
            className={classes.root}
            aria-label="ui-input"
            value={value}
            onChange={onChange}
            placeholder={placeholder}
        />
    );
};

export default Input;
