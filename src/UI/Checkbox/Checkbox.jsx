import React from 'react';

import classes from './Checkbox.module.scss';

const Checkbox = ({ id, label, checked, onChange }) => {
    return (
        <div className={classes.root}>
            <input
                className={classes.checkbox}
                id={id}
                data-testid={id}
                type="checkbox"
                checked={checked}
                onChange={onChange}
            />
            <label className={classes.label} htmlFor={id}>
                {label}
            </label>
        </div>
    );
};

export default Checkbox;
