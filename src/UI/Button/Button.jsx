import React from 'react';

import classes from './Button.module.scss';

const Button = ({ onClick, children, id }) => {
    return (
        <button className={classes.root} data-testid={`button-${id}`} onClick={onClick}>
            {children}
        </button>
    );
};

export default Button;
