let todoData = [
    {
        name: 'First Todo',
        id: 'todo-1',
        complete: false,
    },
    {
        name: 'Second Todo',
        id: 'todo-2',
        complete: false,
    },
    {
        name: 'Third Todo',
        id: 'todo-3',
        complete: false,
    },
];

let count = 4;

const api = {
    getTodoList: async () => todoData,
    addTodoItem: async value => {
        todoData = [...todoData, { id: `todo-${count}`, name: value, complete: false }];
        count += 1;
        return todoData;
    },
    deleteById: async id => {
        todoData = todoData.filter(item => item.id !== id);
        return todoData;
    },
    markComplete: async item => {
        todoData = todoData.map(el => {
            if (el.id === item.id) return item;
            return el;
        });
        return todoData;
    },
};

export default api;
