import React from 'react';

import TodoList from 'src/components/TodoList';

const App = () => {
    return (
        <main>
            <TodoList />
        </main>
    );
};

export default App;
